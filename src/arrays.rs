// Arrays - Fixed list where elements are the same data types

use std::mem;

pub fn run() {
  let mut numbers: [i32; 7] = [1,2,3,4,5,9,12];

  // Re-assign value
  numbers[2] = 50;


  println!("{:?}", numbers);
  
  // Ger single value
  println!("Single value: {}", numbers[2]);

  // Get array length
  println!("Array length: {}", numbers.len());

  // Arrays are stack allocated
  println!("Array occupies {} bytes", mem::size_of_val(&numbers));

  // Get slice
  let slice: &[i32] = &numbers[2..6];
  println!("Slice {:?}", slice)
}