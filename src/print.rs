pub fn run() {
  // Print to console
  println!("Hello from print.rs file :)");

  // Basic formatting
  println!("{} is from {}", "Adam", "Miami" );

  // Positional arguments
  println!("{0} is from {1} and {0} likes to code in {2}.", 
  "John", "Florida", "Rust");

  // Named arguments
  println!("{name} likes to play {activity}.", name="Jamal", activity="basketball");

  // Placeholder traits
  println!("Binary: {:b} Hex: {:x} Octal: {:o}", 5, 5, 5);

  // Placeholder for debug traits
  println!("{:?}", (12, true, "hello"));

  // Basic math
  println!("2 + 3 = {}", 2 + 3);
}