// Vectors - resizable array

use std::mem;

pub fn run() {
  let mut numbers: Vec<i32> = vec![1,2,3,4,5,9,12];

  // Re-assign value
  numbers[2] = 50;

  // Add on to vector
  numbers.push(150);
  numbers.push(250);
  numbers.push(350);

  // Pop off last value
  numbers.pop();


  println!("{:?}", numbers);
  
  // Ger single value
  println!("Single value: {}", numbers[2]);

  // Get Vector length
  println!("Vector length: {}", numbers.len());

  // Vectors are stack allocated
  println!("Vector occupies {} bytes", mem::size_of_val(&numbers));

  // Get slice
  let slice: &[i32] = &numbers[1..3];
  println!("Slice {:?}", slice);

  // Loop through vector values
  for x in numbers.iter() {
    println!("Number: {}", x);
  }

  // Loop through vector and mutate values
  for x in numbers.iter_mut() {
    *x *= 4
  }
  
  println!("Number: {:?}", numbers);
}