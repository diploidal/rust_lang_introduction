pub fn run() {
  greeting("Hello", "Marques");

  // Bind function values to variables
  let get_sum = add(5,5);
  println!("{}", get_sum);

  // Closure
  let n3:i32 = 10;

  let add_nums = |n1:i32, n2:i32| n1 + n2 + n3;
  println!("Closure sum: {}", add_nums(2,3));
}

fn greeting(greet: &str, name: &str) {
  println!("{} {}, nice to meet you !", greet, name);
}


fn add(num_one:i32, num_two:i32) -> i32 {
  num_one + num_two
}