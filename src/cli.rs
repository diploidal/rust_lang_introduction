use std::env;

// Pass arguments to CLI like: cargo run hello world !

pub fn run() {
  let args: Vec<String> = env::args().collect();
  let command = args[1].clone();
  let name = "Anthony";
  let status = "100%";

  // println!("Args: {:?}", args);
  // println!("Command: {:?}", command);

  // cargo run hello
  if command == "hello" {
    println!("Hi {}, how are you ?", name);
  } else if command == "status" {
    println!("status is {}", status);
  } else {
    println!("That's not valud command");
  }
}