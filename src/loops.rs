// Loops - used to iterate until condition is met (as usual)
use std::time::Instant;

pub fn run() {
  let mut count = 0;

  // Infinite loop - unless we break out from it
  // I decided to count the time that it needs to finish loop over some value:P

  let start = Instant::now();
  loop {
    count += 1;
    if count == 1000000 {
      let duration = start.elapsed();
      println!("Iteration from 1 to 1mln took: {:?}", duration);
      break;
    }
  }

  // While loop (FizzBuzz)
  /* 
  while count <= 100 {
    if count % 15 == 0 {
      println!("FizzBuzz");
    } else if count % 3 == 0 {
      println!("Fizz");
    } else if count % 5 == 0 {
      println!("Buzz");
    } else {
      println!("{}", count);
    }
  count += 1;
  }
   */

  /* 
  for x in 0..200 {
    if x % 15 == 0 {
      println!("FizzBuzz");
    } else if x % 3 == 0 {
      println!("Fizz");
    } else if x % 5 == 0 {
      println!("Buzz");
    } else {
      println!("{}", x);
    }
  } 
  */




}