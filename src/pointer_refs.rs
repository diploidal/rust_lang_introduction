// Reference pointers - point to a resource in memory !

pub fn run() {
  // Primitive array
  let array1 = [1,2,3,4,5];
  let array2 = array1;

  println!("{:?}", (array1, array2));


  // with non-primitives, if you ASSIGN another variable to a piece of data, the first variable will no longer hold that value ! You will need to use a reference (&) to point to the resource


  // Vector is non primitive
  let vec1 = vec![6,7,8];
  let vec2 = &vec1;

  // println!("Values from vector: {:?}", (vec1, vec2)); // here vec1 is not visible
  println!("Values from vector: {:?}", (&vec1, vec2)); // here vec1 is npointet as a reference
}