// Primitive str = Immutable fixed-length string somewhere in memory
// String = Growable, heap-allocated data structure - Use when you need to modify or own string data

// Basic string methods

pub fn run() {
  let hello = "Hello"; // Fix-length

  let mut hello_two = String::from("Hello Rust Language "); // Growable

  // Push a char
  hello_two.push('R');
  
  // Push a string
  hello_two.push_str(" pushed string");

  // Capacity in bytes
  println!("Capacity - {} bytes.", hello_two.capacity());
  println!("Is empty - {}", hello_two.is_empty());

  // Contains
  println!("Contains 'Rust'- {}", hello_two.contains("Rust"));

  // Replace
  println!("Replaced - {}", hello_two.replace("Hello", "Heya"));

  // Loop through string by whitespace
  for word in hello_two.split_whitespace() {
    println!("{}", word);
  }

  // Create string with capacity
  let mut my_string = String::with_capacity(5);
  my_string.push('m');
  my_string.push('p');

  println!("{}", my_string);

  // Assertion testing 
  assert_eq!(2, my_string.len());
  println!("{}", my_string);
  assert_eq!(2, my_string.capacity());
  println!("{}", my_string);

  // Get string length
  println!("{:?}", (hello.len(), hello_two.len(), hello_two));
  
}